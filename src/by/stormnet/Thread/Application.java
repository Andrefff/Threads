package by.stormnet.Thread;

public class Application {


    public static void main(String[] args) {
        Application app = new Application();
        app.runTheApp();
    }

    private void runTheApp() {
//        CommonConsoleWrite wrt = new CommonConsoleWrite();
//        MyThread myThread1 = new MyThread("First thread",200l,wrt);
//        MyThread myThread2 = new MyThread("Second Thread",200l,wrt);
//        myThread1.start();
//        myThread2.start();

        Singletone s = Singletone.getInstance();
        System.out.println(s.getInValue());
        Singletone s1 = Singletone.getInstance();
        System.out.println(s1.getInValue());
        s.setInValue(123);
        System.out.println(s.getInValue());
        System.out.println(s1.getInValue());
        System.out.println(Singletone.getInstance().getInValue());

        Singletone.getInstance().setInValue(123123);
        System.out.println(Singletone.getInstance().getInValue());
        System.out.println(s.getInValue());
        System.out.println(s1.getInValue());
    }

}
