package by.stormnet.Thread;

public class MyThread extends Thread {

    private long delay;
    private CommonConsoleWrite write;

    public MyThread(String name, long delay, CommonConsoleWrite write) {
        super(name);
        this.delay = delay;
        this.write = write;
    }

    public void setDelay(long delay) {
        this.delay = delay;
    }

    @Override
    public void run() {

        if(write==null)
            return;
        String name = getName();
        write.write(name);

    }
}
