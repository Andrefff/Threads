package by.stormnet.Thread;

public class Singletone {

    private int inValue;
    private static volatile Singletone instance = null;

    private Singletone() {

    }

    public static Singletone getInstance() {
        if(instance==null){
             synchronized (Singletone.class){
                 if(instance == null){
                     instance = new Singletone();
                 }
             }
        }
        return instance;
    }

    public int getInValue() {
        return inValue;
    }

    public void setInValue(int inValue) {
        this.inValue = inValue;
    }
}
